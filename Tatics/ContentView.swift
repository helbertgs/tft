//
//  ContentView.swift
//  Tatics
//
//  Created by Helbert Gomes on 17/02/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
