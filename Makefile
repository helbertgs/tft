BUNDLE=rbenv exec bundle
LANG_VAR=LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
FASTLANE=$(LANG_VAR) $(BUNDLE) exec fastlane
RUBY=/usr/bin/ruby

bundler:
	bundle install

pods:
	bundle exec pod install

unit:
	$(FASTLANE) unit
