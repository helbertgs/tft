# Property(ies)

slackURL = ""
workspace = ""
crashlyticsToken = ""
crashlyticsSecret = ""

default_platform :ios

platform :ios do

  before_all do |options|
    ENV["SLACK_URL"] = <Slack webhook url>
    ENV["CRASHLYTICS_API_TOKEN"] = @crashlyticsToken
    ENV["CRASHLYTICS_BUILD_SECRET"] = @crashlyticsSecret
  end

  desc "Bumps patch version"
  lane :patch_version do
    increment_version_number(bump_type: "patch")
  end

  desc "Bumps minor version"
  lane :minor_version do
    increment_version_number(bump_type: "minor")
  end

  desc "Bumps major version"
  lane :major_version do
    increment_version_number(bump_type: "major")
  end

  desc "Get github commits as changelogs for beta versions"
  lane :changelogs do
      changelog_from_git_commits(
        commits_count: 5,
        pretty: "%C(auto)%h - %s%w(0,0,4)%n%b",# Optional, lets you provide a custom format to apply to each commit when generating the changelog text
        date_format: "short",# Optional, lets you provide an additional date format to dates within the pretty-formatted string
        match_lightweight_tag: false,  # Optional, lets you ignore lightweight (non-annotated) tags when searching for the last tag
        merge_commit_filtering: "exclude_merges" # Optional, lets you filter out merge commits
      )
  end

  desc "Submit a new Beta Build to Crashlytics"
  desc "This will also make sure the profile is up to date"
  lane :build do |options|
    scheme = (options[:scheme_name] != nil ? options[:scheme_name] : <dev scheme>)
    scan(scheme: scheme)
    # exportOptions = { method: 'development', iCloudContainerEnvironment: 'Development' }
    # gym(scheme:scheme, workspace: @workspace, export_options: exportOptions, silent: true)
    # upload_beta
  end

  lane :unit do 
    build(scheme_name: "Tatics")
  end

  lane :beta do
    build(scheme_name: <dev scheme>)
  end

  desc "Submit a new Beta Build to Apple TestFlight"
  lane :testflight do
    gym(
      scheme: <scheme>,
      export_options: {
        iCloudContainerEnvironment: 'Production'
      },
      xcargs: "-allowProvisioningUpdates"
    ) # Build your app - more options available
    changelogs
    pilot(changelog: lane_context[:FL_CHANGELOG], skip_waiting_for_build_processing: true, distribute_external: true)
  end

  desc "Upload IPA to crashlytics"
  lane :upload_beta do
    changelogs
    crashlytics
    upload_symbols_to_crashlytics
  end

  desc "Update application metadata to ITC"
  lane :metadata_update do
    deliver(app: <appstoreconnect id>, app_identifier: <bundle identifier>, skip_screenshots: true, skip_binary_upload: true, force: true)
  end

  desc "Deploy a new version to the App Store"
  lane :release do
    deploy(should_release: true)
  end

  desc "Deploy a fix version to the App Store"
  lane :deploy do |options|
    should_release = options[:should_release] != nil ? options[:should_release] : false

    gym(
      scheme: <scheme>,
      export_options: {
        iCloudContainerEnvironment: 'Production'
      },
      xcargs: "-allowProvisioningUpdates"
    )
    deliver(force: true, skip_screenshots: true, submit_for_review: true, automatic_release: false)
    tag(should_release: should_release)
  end

  lane :tag do |options|
    should_release = options[:should_release] != nil ? options[:should_release] : false
    if should_release
      version = "#{get_version_number}"
      add_git_tag(tag: version)
      sh "git push origin #{version}"
    end
  end

  after_all do |lane|
    if slackURL != ""
        slack(
        message: lane_context[:FL_CHANGELOG],
        success: true,
        payload: {
            "Build" => get_build_number,
            "Version" => get_version_number(target:<target>)
        },
        default_payloads: [:git_author, :git_branch, :lane]
        )
    end
  end
  
  error do |lane, exception|
    if slackURL != ""
        slack(
        message: exception.message,
        success: false
        )
    end

    notification(
        title: "Error: " + lane,
        message: exception.message
    )
  end
end